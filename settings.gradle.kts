pluginManagement {
    val kotlinVersion: String by settings
    repositories {
        gradlePluginPortal()
    }
    plugins {
        id("org.jetbrains.kotlin.jvm") version kotlinVersion
        id("org.jetbrains.kotlin.js") version kotlinVersion
    }
}


rootProject.name = "kotlin-sandbox"

include("kotlin-js-react")