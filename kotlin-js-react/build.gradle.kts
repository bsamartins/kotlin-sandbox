plugins {
    id("org.jetbrains.kotlin.js")
}

kotlin {
    js {
        browser()
        useCommonJs()
        binaries.executable()
    }
}

repositories {
    maven("https://kotlin.bintray.com/kotlin-js-wrappers/")
    jcenter()
    mavenCentral()


}

dependencies {
    val kotlinReactVersion = "17.0.0-pre.133-kotlin-1.4.21"
    val reactVersion = "17.0.0"
    implementation(kotlin("stdlib-js"))

    //React, React DOM + Wrappers (chapter 3)
    implementation("org.jetbrains:kotlin-react:$kotlinReactVersion")
    implementation("org.jetbrains:kotlin-react-dom:$kotlinReactVersion")
    implementation(npm("react", reactVersion))
    implementation(npm("react-dom", reactVersion))

    //Kotlin Styled (chapter 3)
    implementation("org.jetbrains:kotlin-styled:5.2.0-pre.133-kotlin-1.4.21")
    implementation(npm("styled-components", "~5.2.1"))
//    implementation(npm("inline-style-prefixer", "~6.0.0"))
}