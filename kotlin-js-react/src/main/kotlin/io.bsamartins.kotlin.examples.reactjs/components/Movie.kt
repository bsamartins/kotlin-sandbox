import kotlinx.css.FontWeight
import kotlinx.css.fontWeight
import kotlinx.css.marginLeft
import kotlinx.css.px
import react.RBuilder
import react.RProps
import react.dom.div
import react.rFunction
import styled.css
import styled.styledSpan

external interface MovieProps: RProps {
    var title: String
    var year: Int
}

val Movie = rFunction<MovieProps>("Movie") { props ->
    div {
        styledSpan{
            css {
                fontWeight = FontWeight.bold
            }
            +props.title
        }
        styledSpan {
            css {
                marginLeft = 8.px
            }
            +"(${props.year})"
        }
    }
}

fun RBuilder.movie(title: String, year: Int) = Movie.invoke {
    attrs {
        this.title = title
        this.year = year
    }
}