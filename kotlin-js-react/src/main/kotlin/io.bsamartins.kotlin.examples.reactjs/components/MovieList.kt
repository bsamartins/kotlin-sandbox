import io.bsamartins.kotlin.examples.reactjs.services.Movie
import kotlinx.html.id
import kotlinx.html.js.onChangeFunction
import org.w3c.dom.HTMLSelectElement
import react.RBuilder
import react.RProps
import react.dom.div
import react.dom.option
import react.dom.select
import react.rFunction
import react.useState
import kotlin.math.ceil

external interface MovieListProps: RProps {
    var movies: Array<Movie>
}

val MovieList = rFunction<MovieListProps>("MovieList") { props ->
    val (pageNumber, setPageNumber) = useState(1)
    val (pageSize, setPageSize) = useState(20)
    val pageItems = props.movies.page(page = pageNumber, size = pageSize)
    val pageCount = ceil(props.movies.size / pageSize.toFloat())

    if (pageCount > 0) {
        div {
            select {
                attrs {
                    id = "pageSize"
                    name = "pageSize"
                    value = pageSize.toString()
                    onChangeFunction = {
                        val target = it.target as HTMLSelectElement
                        val newPageSize = target.value.toInt()
                        setPageNumber(1)
                        setPageSize(newPageSize)
                    }
                }
                arrayOf(20, 50, 100).forEach {
                    option(content = it.toString())
                }
            }
        }
        pager(elementCount = props.movies.size, pageNumber = pageNumber, pageSize = pageSize) {
            onChangePageFunction = { setPageNumber(it) }
        }

    }

    for (movie in pageItems) {
        movie(title = movie.title, year = movie.year)
    }
}

fun RBuilder.movieList(movies: Array<Movie>) = MovieList.invoke {
    attrs {
        this.movies = movies
    }
}