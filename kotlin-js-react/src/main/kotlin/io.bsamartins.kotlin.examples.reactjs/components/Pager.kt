import kotlinx.css.*
import kotlinx.html.js.onClickFunction
import react.RBuilder
import react.RProps
import react.dom.a
import react.dom.span
import react.rFunction
import styled.StyleSheet
import styled.css
import styled.styledA
import styled.styledDiv
import kotlin.math.ceil
import kotlin.math.max
import kotlin.math.min

object ComponentStyles : StyleSheet("ComponentStyles", isStatic = true) {
    val selected by css {
        fontWeight = FontWeight.bold
    }
}

external interface PagerProps: RProps {
    var pageNumber: Int
    var pageSize: Int
    var elementCount: Int
    var onChangePageFunction: ((Int) -> Unit)?
}

val Pager = rFunction<PagerProps>("PagerProps") { props ->
    val pageCount = ceil(props.elementCount.toFloat() / props.pageSize.toFloat()).toInt()
    val changePage: (Int) -> Unit = props.onChangePageFunction ?: {}
    val bounds = pageBounds(pageNumber = props.pageNumber, pageCount = pageCount, numberLinks = 10)
    val lowerBound = bounds.first
    val upperBound = bounds.second
    styledDiv {
        css {
            children {
                not("last-child") {
                    marginRight = LinearDimension("5px")
                }
            }
        }
        val firstPageHref = if (props.pageNumber != 1 && pageCount > 1) "#" else null
        val lastPageHref = if (props.pageNumber != pageCount && pageCount > 1) "#" else null

        a(href = firstPageHref) {
            attrs.onClickFunction = { changePage(1) }
            +"<<"
        }
        a(href = firstPageHref) {
            attrs.onClickFunction = { changePage(max(1, props.pageNumber-1)) }
            +"<"
        }
        if (lowerBound > 1) {
            span { +"..." }
        }
        (lowerBound..upperBound).forEach { page ->
            val href = if (page == props.pageNumber) null else "#"
            styledA(href = href) {
                css {
                    if (page == props.pageNumber) {
                        +ComponentStyles.selected
                    }
                }
                attrs.onClickFunction = { changePage(page) }
                +page.toString()
            }
        }
        if (upperBound < pageCount) {
            span { +"..." }
        }
        a(href = lastPageHref) {
            attrs.onClickFunction = { changePage(min(props.pageNumber+1, pageCount)) }
            +">"
        }
        a(href = lastPageHref) {
            attrs.onClickFunction = { changePage(pageCount) }
            +">>"
        }
    }
}

fun RBuilder.pager(pageNumber: Int = 1, pageSize: Int = 20, elementCount: Int, handler: PagerProps.() -> Unit) = Pager.invoke {
    attrs {
        this.pageNumber = pageNumber
        this.pageSize = pageSize
        this.elementCount = elementCount
        handler.invoke(this)
    }

}

fun pageBounds(pageNumber: Int, pageCount: Int, numberLinks: Int): Pair<Int, Int> {
    val lowerBound: Int = if (pageNumber >= (pageCount - numberLinks/2) ) {
        pageCount - numberLinks
    } else {
        pageNumber - numberLinks/2
    }.let { max(1, it) }
    val upperBound = min(lowerBound + numberLinks, pageCount)
    return Pair(lowerBound, upperBound)
}