package io.bsamartins.kotlin.examples.reactjs.components

import io.bsamartins.kotlin.examples.reactjs.services.Movie
import io.bsamartins.kotlin.examples.reactjs.services.MovieService
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import movieList
import react.RProps
import react.dom.h1
import react.rFunction
import react.useEffect
import react.useState

external interface AppProps: RProps

val App = rFunction<AppProps>("App") {
    val (movies, setMovies) = useState<Array<Movie>>(emptyArray())

    useEffect(dependencies = emptyList()) {
        GlobalScope.launch {
            val movies = MovieService.list()
            movies.sort { a, b -> a.title.compareTo(b.title, ignoreCase = true) }
            setMovies(movies)
        }
    }

    h1 { +"Movies" }
    movieList(movies)
}