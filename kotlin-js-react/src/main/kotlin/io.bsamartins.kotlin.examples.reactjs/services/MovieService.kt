package io.bsamartins.kotlin.examples.reactjs.services

import kotlinx.browser.window
import kotlinx.coroutines.await

external interface Movie {
    val title: String
    val year: Int
}

object MovieService {

    suspend fun list(): Array<Movie> {
        return window.fetch("/data/movies.json")
            .await()
            .json()
            .await()
            .unsafeCast<Array<Movie>>()
    }

}