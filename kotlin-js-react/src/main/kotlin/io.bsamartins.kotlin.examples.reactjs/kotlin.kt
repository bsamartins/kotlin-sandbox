import kotlin.math.min

fun <T> Array<T>.page(page: Int, size: Int): Array<T> {
    val start = (page - 1) * size
    val end = min(page * size, this.size) - 1
    return this.sliceArray(start..end)
}