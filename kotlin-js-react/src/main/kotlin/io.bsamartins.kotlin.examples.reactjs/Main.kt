package io.bsamartins.kotlin.examples.reactjs

import io.bsamartins.kotlin.examples.reactjs.components.App
import kotlinx.browser.document
import react.dom.render

fun main() {
    render(document.getElementById("root")) {
        App {}
    }
}